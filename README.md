# TrebleCheck

## Canonical Source

Souce code is located at https://gitlab.com/TrebleInfo/TrebleInfo

## Contributing

Code improvements are welcome! Just submit a merge request to this project.

## Translating

See https://gitlab.com/TrebleInfo/TrebleInfo/-/blob/dev/TRANSLATING.md

## Download

[F-Droid](https://f-droid.org/packages/tk.hack5.treblecheck/)

[GitLab](https://gitlab.com/TrebleInfo/TrebleInfo/-/releases)

[Google Play](https://play.google.com/store/apps/details?id=tk.hack5.treblecheck)

Google Play is a trademark of Google LLC.
